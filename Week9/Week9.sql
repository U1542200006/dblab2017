use bank;

select *
from loan, borrower; 
#this returns cartesian product of 2 tables
#join gives intersections of 2 tables and to use join we need at least 1 common attribute

select *
from loan join borrower on loan.loan_number = borrower.loan_number;

#left outer join gives table which stated at left side + intersection
select * 
from loan left join borrower on loan.loan_number = borrower.loan_number;

#right outer join gives table which stated at right side + intersection
select * 
from loan right join borrower on loan.loan_number = borrower.loan_number;

insert into borrower values ("Esra2", "L-39");
select * 
from loan right outer join borrower on loan.loan_number = borrower.loan_number;
#Primery key for borrower table: combination of customer_name + loan_number
#right outer join=right join, natural join=inner join

#for full outer join we need to combine left join and right join. full set, bec. we don't have full outer join at mySQL

(select *
from loan left join borrower on loan.loan_number = borrower.loan_number)
union
(select * 
from loan right join borrower on loan.loan_number = borrower.loan_number);

(select loan.loan_number, customer_name
from loan left join borrower on loan.loan_number = borrower.loan_number)
union
(select loan.loan_number, customer_name
from loan right join borrower on loan.loan_number = borrower.loan_number);
